function main(varargin)
    startup_lib;
    clc;
    close all;
    
    [a1, a2_a, a2_b, a2_c, a2_d, a3_a, a3_b, a3_c, a3_d, a4] = readXls();
    
    % Question 2. a
    frame = a2_c(1:70, 1);
    right_heel_y = a2_c(1:70, 12);
    [rh_y_max, rh_y_max_idx] = max(right_heel_y);
    [rh_y_min, rh_y_min_idx] = min(right_heel_y);
    frame_max_index = frame(rh_y_max_idx);
    frame_min_index = frame(rh_y_min_idx);
    figure
    plot(frame, right_heel_y, frame_max_index, rh_y_max, 'ro', frame_min_index, rh_y_min, 'go')
    title('Heel Vertical Displacement')
    % 2.b
    heel_displacement = rh_y_max - rh_y_min;
    fprintf('Heel displacement during swing is: %f\n', heel_displacement)
    % 2.c
    hold on
    xline(find(frame == 14))
    xline(find(frame == 27))
    % 2.d
    idx_prev = find(frame == 27);
    idx_curr = find(frame == 29);
    pos_1 = a2_c(idx_prev, 12);
    pos_2 = a2_c(idx_curr, 12);
    time_1 = a2_c(idx_prev, 2);
    time_2 = a2_c(idx_curr, 2);
    HRC_vel = computeVel(pos_1, pos_2, time_1, time_2);
    fprintf('Vertical velocity at HRC: %f\n', HRC_vel)
    %2.e
    idx_prev = find(frame == 27);
    idx_curr = find(frame == 29);
    pos_1 = a2_c(idx_prev, 9);
    pos_2 = a2_c(idx_curr, 9);
    time_1 = a2_c(idx_prev, 2);
    time_2 = a2_c(idx_curr, 2);
    HRC_vel = computeVel(pos_1, pos_2, time_1, time_2);
    fprintf('Horizontal Velocity at HRC: %f\n', HRC_vel)
    %2.f
    % Stride length is the distance from the toe of the foot (starting
    % position) to the toe of the foot (ending position)
    frame = a2_c(:, 1);
    start_pos = find( frame >= 35 & frame < 40 );
    end_pos = find( frame >= 102 & frame <= 106 );
    x_start_pos = a2_c(start_pos, 12);
    x_end_pos = a2_c(end_pos, 12);
    stride_len = mean(x_start_pos - x_end_pos);
    fprintf('Stride Length: %f\n', stride_len)
    %2.g 
    % Horizontal rib cage velocity
    for i=35:104
        frame = a2_a(i, 1);
        pos_1 = a2_a(frame-1, 3);
        pos_2 = a2_a(frame+1, 3);
        time_1 = a2_a(frame-1, 2);
        time_2 = a2_a(frame+1, 2);
        computeVel(pos_1, pos_2, time_1, time_2);
    end
    %5.a
    frame_idx = find(a2_b(:, 1) == 10);
    pos_1 = a2_b(frame_idx-1, 3);
    pos_2 = a2_b(frame_idx+1, 3);
    time_1 = a2_b(frame_idx-1, 2);
    time_2 = a2_b(frame_idx+1, 2);
    frame_vel10 = computeVel(pos_1, pos_2, time_1, time_2);
    fprintf('Knee vel for frame 10: %f\n', frame_vel10)

    %5.b
    frame_idx = find(a2_b(:, 1) == 9);
    pos_1 = a2_b(frame_idx-1, 3);
    pos_2 = a2_b(frame_idx+1, 3);
    time_1 = a2_b(frame_idx-1, 2);
    time_2 = a2_b(frame_idx+1, 2);
    frame_vel9 = computeVel(pos_1, pos_2, time_1, time_2);

    frame_idx = find(a2_b(:, 1) == 11);
    pos_1 = a2_b(frame_idx-1, 3);
    pos_2 = a2_b(frame_idx+1, 3);
    time_1 = a2_b(frame_idx-1, 2);
    time_2 = a2_b(frame_idx+1, 2);
    frame_vel11 = computeVel(pos_1, pos_2, time_1, time_2);

    frame_idx = find(a2_c(:, 1) == 10);
    time_1 = a2_b(frame_idx-1, 2);
    time_2 = a2_b(frame_idx+1, 2);
    acc = computeAcc(frame_vel9, frame_vel11, time_1, time_2);
    fprintf('Knee acc for frame 10: %f\n', acc)

    %5.e
    frame_idx = find(a2_b(:, 1) == 11);
    pos_1 = a2_b(frame_idx-1, 3);
    pos_2 = a2_b(frame_idx+1, 3);
    time_1 = a2_b(frame_idx-1, 2);
    time_2 = a2_b(frame_idx+1, 2);
    frame_vel11 = computeVel(pos_1, pos_2, time_1, time_2);


end